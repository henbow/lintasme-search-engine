<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Generally will be localhost if you're querying from the machine that Mongo is installed on
$config['mongo_host'] = "192.168.1.2";
//$config['mongo_host'] = "192.168.40.129";
//$config['mongo_host'] = "192.168.1.12";


// Generally will be 27017 unless you've configured Mongo otherwise
$config['mongo_port'] = 27017;

// The database you want to work from (required)
$config['mongo_db'] = "lintasberita";

// Leave blank if Mongo is not running in auth mode
$config['mongo_username'] = "";
$config['mongo_password'] = "";

// Persistant connections
$config['mongo_persist'] = TRUE;
$config['mongo_persist_key'] = 'ci_mongo_persist';

// Get results as an object instead of an array
$config['mongo_return'] = 'array'; // Set to object