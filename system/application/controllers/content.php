<?php

class Content extends Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('groonga_model', 'content');
		$this->load->helper('url');
	}
	
	function index() {
		$this->status();
	}
	
	function status() {
		$status = $this->content->get_status();
		
		print '<pre>';
		print_r(json_decode($status));
		print '</pre>';
	}
	
	function table_list() {
		$status = $this->content->table_lists();
		
		print '<pre>';
		print_r(json_decode($status));
		print '</pre>';
	}
	
	function total_records($table) {
		$status = $this->content->total_records($table);
		
		print '<pre>';
		print_r(json_decode($status));
		print '</pre>';
	}
	
	function column_list($table) {
		$status = $this->content->column_lists($table);
		
		print '<pre>';
		print_r(json_decode($status));
		print '</pre>';
	}
	
	function add_content() {
		$insert = '';	
		if($this->input->post('submit')) {
			$data = array(
						array(
							'_key' => $this->input->post('title'),
							'content' => iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($this->input->post('content')))
						)
					);
			
			$insert = $this->content->insert('related_post', $data);
		}
		
		echo '<strong>Insert To Groonga</strong><br><form method="post" action="'.current_url().'">';
		echo '<input type="text" name="title" size="50" /><br>';
		echo '<textarea name="content" rows="30" cols="80"></textarea><br><input type="submit" name="submit" value="Add Content" /><br>';
		echo '</form>';
		echo '<pre>';
		echo $insert;
		echo '</pre>';
	}
	
	function add_title() {
		$insert = '';
		if($this->input->post('submit')) {
			$title = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($this->input->post('title')));
			$data = array(
						array(
							'_key' => md5($title),
							'title' => $title
						)
					);
			
			$insert = $this->content->insert('related_post',$data);
		}
		
		echo '<strong>Add Title</strong><br><form method="post" action="'.current_url().'">';
		echo '<input type="text" name="title" size="50" /><br>';
		echo '<input type="submit" name="submit" value="Add" /><br>';
		echo '</form>';
		echo '<pre>';
		echo $insert;
		echo '</pre>';
	}

	function search($mode = 'regular') {
		date_default_timezone_set('Asia/Jakarta');
				
		$result = '';
		$query = '';
		$type = '';		
		
		if($this->input->post('submit')) {
			$query = $this->input->post('query');
			$type = $this->input->post('stype');
			//$total_stopwords = $this->content->total_stopwords();
			$stopwords = $this->content->get_stopwords();
			
			if($mode == 'regular') {
				$rs = json_decode($this->content->regular_search($query, 'and', $stopwords));
				$result = array();
				$result['exec_time'] = $rs[0][2].' seconds';
				
				if(isset($rs[1])){
					$res = $rs[1][0];
					$result['total_results'] = count($res) - 2;
					
					for ($i=2; $i < count($res); $i++) {
						$result[] = array(
							'id' => $res[$i][1],
							'creation_date' => date('Y-m-d H:i:s', intval($res[$i][2])),
							'title' => $res[$i][11],
							'message' => $res[$i][6],
							'domain' => $res[$i][8],
							'writer' => $res[$i][12],
							'url' => $res[$i][5]
						);
					}
				}
			} else {
				$res = $this->content->suggestion($data);
			}
		}
		
		echo '<strong>Search</strong><br><form method="post" action="'.current_url().'">';
		echo '<input type="text" name="query" size="80" value="'.$query.'" />&nbsp;';
		/*
		echo '<select name="stype">
				<!--<option value="" '.($type==''?'selected="selected"':'').'>Exact</option>-->
				<option value="OR" '.($type=='OR'?'selected="selected"':'').'>OR</option>
				<option value="AND" '.($type=='AND'?'selected="selected"':'').'>AND</option>
			  </select><br>';
		*/
		echo '<input type="submit" name="submit" value="Search" /><br>';
		echo '</form>';
		echo '<pre>';
		//print_r($rs);
		print_r($result);
		echo '</pre>';
	}

	function add_stopwords() {		
		if($this->input->post('submit')) {
			$word = trim($this->input->post('words'));
			$data = array();
					
			$data[] = array(
							'_key' => md5($word),
							'worditem' => $word
						);		
			
			$add = $this->content->insert('stopwords',$data);
		}
		
		$words = $this->content->get_stopwords();
		
		echo '<strong>Stopwords List</strong><br><form method="post" action="'.current_url().'">';
		echo '<input type="text" name="words" value="" /><input type="submit" name="submit" value="Add Stopwords" />';
		echo '</form>';
		echo 'Stopwords count: <strong>' . $this->content->total_stopwords() . ' words</strong><br>';
		
		foreach ($words as $word) {
			echo $word.' '.anchor('content/delete_stopwords/'.md5($word),'[x]').'<br>';
		}
	}
	
	function delete_stopwords($word) {		
		$this->content->delete('stopwords', $word);
		
		redirect('content/add_stopwords');
	}

	/**
	 * Insert/migrate stopwords from redis to groonga. Temporary function.
	 * @access public
	 */
	 /*
	function add_stopwords() {
		$this->load->model('tag_model', 'tag');
		$stopwords = $this->tag->get_stopwords();		
		$data = array();
		
		foreach ($stopwords as $value) {
			$data[] = array(
						'_key' => md5($value),
						'worditem' => $value
					);
		}
		
		echo '<pre>';
		//print_r($data);die;
		
		print_r($this->content->select_table('stopwords'));
		echo '</pre>';
	}
	  * 
	  */
}
