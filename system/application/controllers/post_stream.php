<?php
class Post_stream extends Controller {
	var $limit = 10;
	var $score_threshold = 10;
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('groonga_model', 'groongam');
		$this->load->helper('url');
		$this->load->helper('json');
	}
	
	function index() {
		echo "Haloooo....";
		//$this->migrate();
	}
	
	/**
	 * Used for migration data only from mongodb
	 * @access public
	 * @param string Last inserted ID. Optional.
	 * @return string Next ID
	 */
	function migrate($id = '') {
            // return FALSE;

            if($id == '') {
                $the_id = json_decode($this->groongam->get_lastid());

                $nextid = isset($the_id[1][0][2][1]) ? $the_id[1][0][2][1] : '4f729191e0c8babd5700019f';
            } else {
                $nextid = $id;
            }

            $result = '';
            $url    = '';
            $insert = '';
            $data   = array();

            $ch  = curl_init();
            curl_setopt($ch,CURLOPT_URL, 'http://www.lintas.me/tester/groonga_migrate/'.$nextid.'/100'); 
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
            $result = json_decode(curl_exec($ch));
            curl_close($ch);
		
            if(isset($result->stream)) {
                foreach ($result->stream as $res) {				
                    $data[] = array (
                            '_key' => $res->id,
                            'creation_date' => $res->creation_date,
                            'deleted' => FALSE,
                            'insert_dt' => microtime(TRUE),
                            'link_url' => $res->link_url,
                            'message' => trim($res->message),
                            'popular_post' => FALSE,
                            'source' => $res->domain,
                            'tag' => implode(',', $res->tag),
                            'tipe' => $res->type,
                            'title' => $res->title,
                            'writer' => $res->creator_name
                    );
                }

                $insert = $this->groongam->insert('post_stream', $data);
                $last_id = str_replace('http://www.lintas.me/tester/groonga_migrate/', '', $result->next);

                echo $last_id;
            }
	}
	
	/**
	 * Method for searching
	 * @link http://117.102.103.166/?c=post_stream&m=search&query=<search_keywords>
	 * @access public
	 * @method GET
	 * @param string query. Search keywords.
	 * @param integer page. Results offset.
	 * @param integer limit. Set limit results.
	 * @param boolean pretty_print. Whether use JSON pretty print or not. 
	 * @return JSON array
	 * Return example:
	 * {
	 * 		"results": 
	 * 		{
	 * 			"exec_time": "0.19323968887329 seconds",
	 * 			"total_records": 1,
	 * 			"items": 
	 * 			{
	 * 				"0": 
	 * 				{
	 * 					"id": "512f2818e0c8ba517d0038fa",
	 * 					"creation_date": 1362045483,
	 * 					"title": "'Blackberry Porsche di banderol 21 Juta di Indonesia  '",
	 * 					"tag": "'gadget' ",
	 * 					"message": "'\\n\\nBlackberry Poche akhirnya tiba di Indonesia dengan harga yang fantastis, yaitu Rp21 jutaan. Apa yang membuat poel ini memiliki harga semahal itu? Ini karena Blackberry Poche dibuat menggunakan material yang baik, produksi yang terbatas dan lebih mengedepankan seni. Meski harganya ...'",
	 * 					"domain": "kabardigital.com",
	 * 					"writer": "50ba6ef1e0c8ba583d00512c",
	 * 					"url": "http:\/\/www.kabardigital.com\/545\/blackberry-porsche-di-banderol-21-juta-di-indonesia.html"
	 * 				}
	 * 			}
	 * 		}
	 * }
	 */
	function search() {
            $start  = microtime(TRUE);
            $query  = isset($_GET['query']) ? $_GET['query'] : '';
            $label  = isset($_GET['label']) ? $_GET['label'] : '';
            $page   = isset($_GET['page']) ? ($_GET['page'] == 0 ? 0 : $_GET['page'] - 1) : '';
            $limit  = isset($_GET['limit']) ? $_GET['limit'] : $this->limit;
            $pretty = isset($_GET['pretty_print']) ? ( $_GET['pretty_print'] == 'true' ? TRUE : FALSE ) : FALSE;
            $result = array();
            $data   = '';
            $type   = '';

            $rs  = json_decode($this->groongam->regular_search($query, $label, $limit, $page));
            $end = microtime(TRUE);
            $result['query'] = $query;
            $result['query_time'] = $rs[0][2];
            $result['query_found'] = $rs[1][0][0][0];
            $result['total_page'] = ceil(intval($result['query_found']) / $limit);

            if(strlen($query)) {
                if(isset($rs[1])){
                    $rset = $rs[1][0];
                    for ($i = 2; $i < count($rset); $i++) {					
                        $result['result'][] = array (
                            'id' => $rset[$i][0],
                            'article_id' => $rset[$i][1],
                            'source' => $rset[$i][9],
                            'insert_dt' => date('Y-m-d H:i:s', intval($rset[$i][5])),
                            'title' => trim($rset[$i][12], '\'" '),
                            'message' => trim($rset[$i][7], '\'" '),
                            'deleted' => intval($rset[$i][3]),
                            'popular_post' => intval($rset[$i][8]),
                            'writer' => $rset[$i][13],
                            'creation_date' => date('Y-m-d H:i:s', intval($rset[$i][2])),
                            'link_url' => $rset[$i][6],
                            'tag' => $rset[$i][10],
                            'insert_by' => $rset[$i][4],
                            'wrap_message' => "", //count($rset[$i][13]) > 0 ? implode('...', $rset[$i][13]) . '...' : $rset[$i][13] . '...',
                            'wrap_title' => ""
                            //'score' => $rset[$i][13]
                        );
                    }
                }
            }

            $this->__response($result, $pretty);
	}

	/**
	 * Method for searching posts from crawler engine
	 * @link http://117.102.103.166/?c=post_stream&m=search&query=<search_keywords>
	 * @access public
	 * @method GET
	 * @param string query. Search keywords.
	 * @param string label. Search by title, message, tag or source. Default blank.
	 * @param integer page. Results offset.
	 * @param integer limit. Set limit results.
	 * @param boolean pretty_print. Whether use JSON pretty print or not. 
	 * @return JSON array
	 * Return example:
	 * {
	 * 		"results": 
	 * 		{
	 * 			"exec_time": "0.19323968887329 seconds",
	 * 			"total_records": 1,
	 * 			"items": 
	 * 			{
	 * 				"0": 
	 * 				{
	 * 					"id": "512f2818e0c8ba517d0038fa",
	 * 					"creation_date": 1362045483,
	 * 					"title": "'Blackberry Porsche di banderol 21 Juta di Indonesia  '",
	 * 					"tag": "'gadget' ",
	 * 					"message": "'\\n\\nBlackberry Poche akhirnya tiba di Indonesia dengan harga yang fantastis, yaitu Rp21 jutaan. Apa yang membuat poel ini memiliki harga semahal itu? Ini karena Blackberry Poche dibuat menggunakan material yang baik, produksi yang terbatas dan lebih mengedepankan seni. Meski harganya ...'",
	 * 					"domain": "kabardigital.com",
	 * 					"writer": "50ba6ef1e0c8ba583d00512c",
	 * 					"url": "http:\/\/www.kabardigital.com\/545\/blackberry-porsche-di-banderol-21-juta-di-indonesia.html"
	 * 				}
	 * 			}
	 * 		}
	 * }
	 */
	function search_from_engine() {
		$start  = microtime(TRUE);
		$query  = isset($_GET['query']) ? $_GET['query'] : '';
		$label  = isset($_GET['label']) ? $_GET['label'] : '';
		$page   = isset($_GET['page']) ? ($_GET['page'] == 0 ? 0 : $_GET['page'] - 1) : '';
		$limit  = isset($_GET['limit']) ? $_GET['limit'] : $this->limit;
		$pretty = isset($_GET['pretty_print']) ? ( $_GET['pretty_print'] == 'true' ? TRUE : FALSE ) : FALSE;
		$result = array();
		$data   = '';
		$type   = '';
		
		$rs = json_decode($this->groongam->engine_search($query, $label, $limit, $page));
		$end = microtime(TRUE);
		$result['query'] = $query;
		$result['query_time'] = $rs[0][2];
		$result['query_found'] = $rs[1][0][0][0];
		$result['total_page'] = ceil(intval($result['query_found']) / $limit);
				
		if(strlen($query)) {
			if(isset($rs[1])){
				$rset = $rs[1][0];
				for ($i = 2; $i < count($rset); $i++) {					
					$result['result'][] = array (
						'id' => $rset[$i][0],
						'article_id' => $rset[$i][1],
						'source' => $rset[$i][9],
						'insert_dt' => date('Y-m-d H:i:s', intval($rset[$i][5])),
						'title' => trim($rset[$i][12], '\'" '),
						'message' => trim($rset[$i][7], '\'" '),
						'deleted' => intval($rset[$i][3]),
						'popular_post' => intval($rset[$i][8]),
						'writer' => $rset[$i][13],
						'creation_date' => date('Y-m-d H:i:s', intval($rset[$i][2])),
						'link_url' => $rset[$i][6],
						'tag' => $rset[$i][10],
						'insert_by' => $rset[$i][4],
						'wrap_message' => "", //count($rset[$i][13]) > 0 ? implode('...', $rset[$i][13]) . '...' : $rset[$i][13] . '...',
						'wrap_title' => ""
						//'score' => $rset[$i][13]
					);
				}
			}
		}
		
		$this->__response($result, $pretty);
	}

	function search_by_tag() {
		$start  = microtime(TRUE);
		
		$query  = isset($_GET['tag']) ? $_GET['tag'] : '';
		$page   = isset($_GET['page']) ? ($_GET['page'] == 0 ? 0 : $_GET['page'] - 1) : '';
		$limit  = isset($_GET['limit']) ? $_GET['limit'] : $this->limit;
		$pretty = isset($_GET['pretty_print']) ? ( $_GET['pretty_print'] == 'true' ? TRUE : FALSE ) : FALSE;
		$result = array();
		$data   = '';
		$type   = '';
		
		$rs = json_decode($this->groongam->tag_search($query, $limit, $page));
		
		$end  = microtime(TRUE);
		$result['query'] = $query;
		$result['query_time'] = $rs[0][2];
		$result['query_found'] = $rs[1][0][0][0];
		$result['total_page'] = ceil(intval($result['query_found']) / $limit);
		$result['result'] = array();
		
		if(strlen($query)) {
			if(isset($rs[1])){
				$rset = $rs[1][0];
				for ($i = 2; $i < count($rset); $i++) {					
					$result['result'][] = array (
						'id' => $rset[$i][0],
						'article_id' => $rset[$i][1],
						'source' => $rset[$i][9],
						'insert_dt' => date('Y-m-d H:i:s', intval($rset[$i][5])),
						'title' => trim($rset[$i][12], '\'" '),
						'message' => trim($rset[$i][7], '\'" '),
						'deleted' => intval($rset[$i][3]),
						'popular_post' => intval($rset[$i][8]),
						'writer' => $rset[$i][13],
						'creation_date' => date('Y-m-d H:i:s', intval($rset[$i][2])),
						'link_url' => $rset[$i][6],
						'tag' => $rset[$i][10],
						'insert_by' => $rset[$i][4],
						'wrap_message' => "", //count($rset[$i][13]) > 0 ? implode('...', $rset[$i][13]) . '...' : $rset[$i][13] . '...',
						'wrap_title' => "",
						//'score' => $rset[$i][13]
					);
				}
			}
		}
		
		$this->__response($result, $pretty);
	}
	
	/**
	 * Method to get related post
	 * @link http://117.102.103.166/?c=post_stream&m=related&query=<search_keywords>
	 * @access public
	 * @method GET
	 * @param string query. Search keywords.
	 * @param integer page. Results offset.
	 * @param integer limit. Set limit results.
	 * @param boolean pretty_print. Whether use JSON pretty print or not.
	 * @return JSON array
	 * 
	 */
	function related() {
		$start  = microtime(TRUE);
		$query  = isset($_GET['query']) ? $_GET['query'] : '';
		$tagname = isset($_GET['tagname']) ? $_GET['tagname'] : '';
		$page   = isset($_GET['page']) ? ($_GET['page'] == 0 ? 0 : $_GET['page'] - 1) : '';
		$limit  = isset($_GET['limit']) ? $_GET['limit'] : $this->limit;
		$pretty = isset($_GET['pretty_print']) ? ( $_GET['pretty_print'] == 'true' ? TRUE : FALSE ) : FALSE;
		$result = array();
		$data   = '';
		$type   = '';
		// $query  = $query == '' ? $this->input->post('query') : $query;
		$stopwords = $this->groongam->get_stopwords();
		$rs = json_decode($this->groongam->related_post($query, $tagname, 'OR', $stopwords, $limit, $page));
		$end  = microtime(TRUE);
		$result['query'] = $query;
		$result['query_time'] = $rs[0][2];
		$result['query_found'] = $rs[1][0][0][0];
		$result['total_page'] = ceil(intval($result['query_found']) / $limit);
	
		if(strlen($query)) {
			if(isset($rs[1])){
				$rset = $rs[1][0];
				for ($i = 2; $i < count($rset); $i++) {
					if($rset[$i][13] < $this->score_threshold){
						$result['result'][] = array (
								'id' => $rset[$i][0],
								'article_id' => $rset[$i][1],
								'source' => $rset[$i][9],
								'insert_dt' => date('Y-m-d H:i:s', intval($rset[$i][5])),
								'title' => trim($rset[$i][12], '\'" '),
								'link_url' => $rset[$i][6],
								'tag' => $rset[$i][10],
								'insert_by' => $rset[$i][4],
								'score' => $rset[$i][14]
						);
					}
				}
			}
		}
	
		$this->__response($result, $pretty);
	}
	
	/**
	 * Method for inserting data
	 * @link http://117.102.103.166/?c=post_stream&m=insert
	 * @access public
	 * @method POST
	 * @param string article_id
	 * @param string source
	 * @param string title
	 * @param string link_url
	 * @param string message
	 * @param boolean deleted
	 * @param boolean popular_post
	 * @param string tipe
	 * @param string writer
	 * @param integer creation_date
	 * @param string tag
	 * @param string insert_by
	 * @return JSON array
	 * Return example for SUCCESS:
	 * {
	 * 		"response": 
	 * 		{
	 * 			"status": "SUCCESS",
	 * 			"article_id": "512f2818e0c8ba517d0038fa",
	 * 			"message": "1 article inserted"
	 * 		}
	 * }
	 * 
	 * Return example for FAILED:
	 * {
	 * 		"response": 
	 * 		{
	 * 			"status": "FAILED",
	 * 			"article_id": "",
	 * 			"message": "Failed insert article"
	 * 		}
	 * }
	 */
	function insert() {
		$id = $this->input->post('article_id');
		$source = $this->input->post('source');
		$insert_date = time(); //microtime(TRUE);
		$title = $this->input->post('title');
		$link_url = $this->input->post('link_url');
		$message = $this->input->post('message');
		$deleted = $this->input->post('deleted');
		$popular_post = $this->input->post('popular_post');
		$type = $this->input->post('tipe');
		$writer = $this->input->post('writer');
		$creation_date = $this->input->post('creation_date');
		$tag = $this->input->post('tag');
		$insert_by = $this->input->post('insert_by') == '' ? 'web' : 'engine';
		
		$data = array(
			'_key' => $id,
			'creation_date' => $creation_date,
			'deleted' => $deleted,
			'insert_dt' => $insert_date,
			'link_url' => $link_url,
			'message' => $message,
			'popular_post' => $popular_post,
			'source' => $source,
			'tag' => $tag,
			'tipe' => $type,
			'title' => $title,
			'writer' => $writer,
			'insert_by' => $insert_by
		);
		
		if($id == '') {
			$insert = array('response' => array(
				'status' => 'FAILED',
				'article_id' => '',
				'message' => 'Empty request'
			));
		} else {
			$resp = json_decode($this->groongam->insert('post_stream', $data));
			
			if($resp[1] == '1') {
				$insert = array('response' => array(
					'status' => 'SUCCESS',
					'article_id' => $id,
					'message' => '1 article inserted'
				));
			} else {
				$insert = array('response' => array(
					'status' => 'FAILED',
					'article_id' => '',
					'message' => 'Failed insert article'
				));
			}
		}
		
		$this->__response($insert);
	}

	/**
	 * Method for deleting record (soft delete)
	 * @access public
	 * @method POST
	 * @param string article_id
	 * @param boolean set delete status. TRUE or FALSE.
	 * @return JSON array
	 * Return example for SUCCESS:
	 * {
	 * 		"response": 
	 * 		{
	 * 			"status": "SUCCESS",
	 * 			"message": "1 article deleted"
	 * 		}
	 * }
	 * 
	 * Return example for FAILED:
	 * {
	 * 		"response": 
	 * 		{
	 * 			"status": "FAILED",
	 * 			"message": "Failed delete article"
	 * 		}
	 * }
	 */
	function delete() {
            $id = $this->input->post('article_id');
            $deleted = $this->input->post('deleted');

            $data = array(
                '_key' => $id,
                'deleted' => $deleted
            );

            if($id != '' && $deleted != '') {
                $resp = json_decode($this->groongam->insert('post_stream', $data));

                if($resp[1] == '1') {
                    $insert = array('response' => array(
                        'status' => 'SUCCESS',
                        'message' => '1 article deleted'
                    ));
                } else {
                    $insert = array('response' => array(
                        'status' => 'FAILED',
                        'message' => 'Failed delete article'
                    ));
                }
            } else {
                $insert = array('response' => array(
                    'status' => 'FAILED',
                    'message' => 'Empty request.'
                ));
            }

            $this->__response($insert);
	}
	
	/**
	 * Process array that returned by API method to JSON
	 * @access private
	 * @param array
	 * @param boolean Is pretty print?
	 * @return JSON
	 */
	function __response($result, $is_pretty_print = FALSE) {
		$this->output->set_header("HTTP/1.0 200 OK");
		$this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Content-Type: application/json");
		if($is_pretty_print === FALSE) {
			$this->output->set_output(json_encode($result));
		} else {
			$this->output->set_output(json_readable_encode($result));
		}
	}
}
