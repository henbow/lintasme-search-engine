<?php
class GroongaHTTPUtil{
    
    /**
     * This Class is Utility for Groonga with HTTP.
     * 
     * Groonga is an open-source fulltext search engine and column store. 
     * For detail of Groonga , see following web site.
     * http://groonga.org/
     * 
     */
    
    const orConditionTerm    = ' OR ';
    const andConditionTerm   = '+';
    const minusConditionTerm = ' - ';
    
    private $host            = ''; 
    private $port            = '';
    private $table           = '';
    private $query           = '';
    private $key			 = '';
    private $limit			 = 100;
    private $offset			 = 0;
    private $outputColumns   = '';
    private $matchColumns	 = '';
    private $sortby          = '';
    private $format          = '';
    private $data			 = '';
    private $filter			 = '';
    private $suggestFields   = '';
    private $suggestColumn   = '';
    private $commandVersion  = '';

    /**
     * Class constructor
     * @access public
     * @param array Array('host' => '127.0.0.1', 'port' => 10041, 'format' => 'JSON' or 'XML' or 'TSV' or '')
     */
    public function __construct($params){            
        if($params['host'] == null){
            return FALSE;
        }
        
        $this->host     = $params['host'];
        $this->port     = $params['port'];
        $this->format   = $params['format'];
    }
	
    public function resetAll() {
            $this->table = '';
            $this->query = '';
            $this->key = '';
            $this->limit = 100;
            $this->offset = 0;
            $this->outputColumns = '';
            $this->matchColumns = '';
            $this->sortby = '';
            $this->suggestFields = '';
            $this->suggestColumn = '';
    }

    /**
     * Return groonga server hostname
     * @access public
     * @return string
     */
    public function getHost(){
        return $this->host;
    }
    
    /**
     * Return groonga server port number
     * @access public
     * @return string
     */
    public function getPort(){
        return $this->port;
    }
    
    /**
     * Set table for query
     * @access public
     * @param string table name
     */
    public function setTable($table){
        $this->table = $table;
    }
   
    /**
     * Get table name
     * @access public
     * @return string table name
     */
    public function getTable(){
        return $this->table;
    }
	
    /**
     * Set fields for suggestion
     * @access public
     * @param array field names
     */
    public function setSuggestField($fields){
        $this->suggestFields = implode(',', $fields);
    }
	
    /**
     * Get field names for suggestion
     * @access public
     * @return string
     */
    public function getSuggestField() {
        return $this->suggestFields;
    }
    
    public function setQuery($query){
        $this->query = $query;
    }
    
    public function getQuery(){
        return $this->query;
    }
	
    public function setKey($key){
        $this->key = $key;
    }
    
    public function getKey(){
        return $this->key;
    }
    
    /**
     * Output format in JSON, XML, or TSV
     * @return string
     */
    public function getFormat(){
        return $this->format;
    }
    
    /**
     * Set output format in JSON, XML, or TSV
     * @param string
     */
    public function setFormat($format){
        $this->format = $format;
    }
    
    public function getSortby(){
        return $this->sortby;
    }
    
    public function setSortBy($sortby){
        $this->sortby = $sortby;
    }
	
    public function setLimit($limit, $offset = 0) {
        $this->limit = $limit;
        $this->offset = $offset;
    }
	
    public function getLimit() {
        return $this->limit;
    }
	
    /**
     * Get offset number (page number)
     */
    public function getOffset() {			
        return $this->offset;
    }

    public function setFilter($filter) {
        $this->filter = $filter;
    }

    /**
     * Get insert data
     * @access public
     * @return mixed array or string
     */
    public function getInsertData() {
        return $this->data;
    }

    /**
     * Set insert data
     * @access public
     * @param array data to insert
     * @return void
     */
    public function setInsertData($data) {
        $this->data = $data;
    }

    /**
     * Get condition terms (OR, AND, or MINUS)
     * @access public
     * @param string
     * @return string
     */
    public function getConditionTerm($condition){
        $returnConditionTerm = '';
        
        $condition = trim($condition);
        
        switch($condition){
            case 'AND':
            case 'and':
                $returnConditionTerm = self::andConditionTerm;
                break;                
            case 'OR':
            case 'or':
                $returnConditionTerm = self::orConditionTerm;
                break;
            case 'MINUS':
            case 'minus':
                $returnConditionTerm = self::minusConditionTerm;
                break;
            default:
                $returnConditionTerm = self::andConditionTerm;
                break;
        }
        
        return $returnConditionTerm;
    }
    
    /**
	 * Set query with conditions
	 * @access public
	 * @param array 
	 * @param string
	 */
    public function setCondition($queries , $condition = 'AND'){
        if( !is_array($queries) ){
            return FALSE;
        }
        
        $conditionTerm = $this->getConditionTerm($condition);
        
        if($this->query != ''){
            $this->query .= $conditionTerm;
        }
        
        foreach($queries as $query){
            $this->query .= ( $query . $conditionTerm );
        } 
        
        $this->query = rtrim( $this->query , $conditionTerm);
    }
    
    /**
     * Reset condition terms
	 * @access public
     */
    public function resetCondition(){
         $this->query = '';
    }
	
    /**
     * Set column
     * @access public
     */
    public function setSuggestColumn($column) {
        $this->suggestColumn = $column;
    }

    /**
     * @access public
     */
    public function getSuggestColumn() {
        return $this->suggestColumn;
    }

    /**
     * Set column names for match_column paramater for search
     * @access public
     * @param array
     */
    public function setMatchColumn($column) {
        if(is_array($column)) {
            $this->matchColumns = implode('||', $column);
        } else {
            $this->matchColumns = $column;
        }
    }

    /**
     * Get column names for match_column paramater for search
     * @access public
     * @return string
     */
    public function getMatchColumn() {
        return $this->matchColumns;
    }

    public function setCommandVersion($version) {
        $this->commandVersion = $version;
    }

    public function getCommandVersion($version) {
        return $this->commandVersion;
    }
    
    /**
	 * Set output columns
	 * @access public
	 * @param array columns name
	 * @return void
     */
    public function setOutputColumns($Columns){
        if(  !is_array($Columns) ){
            return FALSE;
        }
          
        if($this->outputColumns != ''){
            $this->outputColumns .= ',';
        }
        
        $this->outputColumns .= implode(',' , $Columns); 
    }
    
    /**
     * Reset output columns
	 * @access public
	 * @return void
     */
    public function resetOutputColumns(){
        $this->outputColumns = '';
    }
    
	/**
	 * Get output columns
	 * @return mixed array or string
	 */
    public function getOutputColumns(){
        return $this->outputColumns;
    }
	
    /**
     * Get server status
     * @access public
     * @return JSON
     */
    public function getServerStatus() {
        $parameter = '/d/status.' . $this->format;
        $result = $this->doRequest($parameter);

        return $result;
    }
	
    /**
     * Get table list of groonga server
     * @access public
     * @return JSON
     */
    public function getTableList(){
        $parameter = '/d/table_list.' . $this->format;
        $result = $this->doRequest($parameter);

        return $result;
    }

    /**
     * Get column list from a table
     * @access public
     * @param string table name
     * @return JSON
     */
    public function getColumnList($table){
        $parameter = '/d/column_list.' . $this->format . '?table='.$table;
        $result = $this->doRequest($parameter);

        return $result;
    }
    
    /**
     * Excuse select query.
     * @access public
     * @return JSON
     */
    public function doSelect($keyword = '') {
        if($this->table == '' ) {
            return FALSE;
        }
        
        $parameter = '/d/select.' . $this->format . '?table=' . $this->table;
		
        if($this->matchColumns != '') {
            $parameter .= ( '&match_columns=' . $this->matchColumns );
        }
        
        if($this->query != ''){
            $parameter .= ( '&query=' . str_replace(' ', '%20', $this->query) );
        }
        
        if($this->outputColumns != ''){
            $parameter .= ( '&output_columns=' . $this->outputColumns );
        }
        
        if($this->sortby != ''){
            $parameter .= ( '&sortby=' . $this->sortby );
        }
		
        if($this->commandVersion != '') {
            $parameter .= ( '&command_version=' . $this->commandVersion );
        }

        if($this->filter != '') {
            $parameter .= ( '&filter=' . $this->filter );
        }

        $parameter .= ( '&limit=' . $this->limit . '&offset=' . $this->offset . '&cache=yes' );
        $result     = $this->doRequest($parameter);
        
        return $result;
    }
	
    /**
     * Do suggestion search
     * @access public
     * @return JSON
     */
    public function doSuggest() {
        if($this->table == '' ){
            return FALSE;
        }

        $parameter = '/d/suggest.' . $this->format . '?table=' . $this->table;

        if($this->suggestColumn != ''){
            $parameter .= ( '&column=' . $this->suggestColumn );
        }

                $parameter .= "&types=suggest&frequency_threshold=1";

        if($this->query != ''){
            $parameter .= ( '&query=' . $this->query );
        }

        $result = $this->doRequest($parameter);

        return $result;
    }

    /**
     * Insert data to table
     * @access public
     * @return JSON
     */
    public function doInsert() {
        if($this->table == '' ){
            return FALSE;
        }

        $parameter = '/d/load.' . $this->format . '?table=' . $this->table;

        if($this->data != ''){
            $parameter .= ( '&values=' . urlencode( json_encode($this->data)) );
        }

        return $this->doRequest($parameter);
    }

    /**
     * Insert learning data for suggestion
     * @access public
     * @return JSON 
     */
    public function doInsertLearningData() {
        if($this->table == '' ){
            return FALSE;
        }

        $parameter = '/d/load.' . $this->format . '?table=' . $this->table;

        if($this->data != '') {
            $parameter .= ( '&each='.urlencode('suggest_preparer('.$this->suggestFields.')').'&values=' . urlencode(json_encode($this->data)) );
        }

        // echo $parameter;die;

        return $this->doRequest($parameter);
    }

    /**
     * Delete data/records from table
     * @access public
     * @param 
     */
    public function doDelete() {
        if($this->table == '' ){
            return FALSE;
        }

        $parameter = '/d/delete.' . $this->format . '?table=' . $this->table;

        if($this->key != ''){
            $parameter .= ( '&key=' . $this->key );
        }

        return $this->doRequest($parameter);
    }
    
    /**
     * Send request to groonga server
     * @access private
     * @param string serialized GET paramaters
     * @return JSON response from server
     */
    private function doRequest($parameter){            
        $ch = curl_init();
	
        // echo ('http://' . $this->host .':' . $this->port . $parameter).'\n';die;
	
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FAILONERROR, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
        curl_setopt($ch, CURLOPT_URL, 'http://' . $this->host .':' . $this->port . $parameter);
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
}