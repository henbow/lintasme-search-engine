<?php
/**
 * Groonga Model Class
 * @author Hendro (hendro@lintas.me)
 * 
 */
 
class Groonga_model extends Model {
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		
		$groonga_conf = array(
							'host' => 'search.lintas.me', //'103.26.101.21', //'search.lintas.me',
							'port' => 10041,
							'format' => 'json'
						);
						
		$this->load->library('GroongaHTTPUtil', $groonga_conf, 'groonga');
	}
	
	/**
	 * Get server status
	 * @access public
	 * @return JSON
	 */
	function get_status() {
		return $this->groonga->getServerStatus();
	}
	
	/**
	 * Get table list of groonga server
	 * @access public
	 * @return JSON
	 */
	function table_lists($table) {
		$this->groonga->resetAll();		
		return $this->groonga->getTableList();
	}
	
	/**
	 * Get column list of table
	 * @access public
	 * @param string table name
	 * @return JSON column lists
	 */
	function column_lists($table) {
		$this->groonga->resetAll();
		return $this->groonga->getColumnList($table);
	}
	
	/**
	 * Select records from table
	 * @access public
	 * @param string table name
	 * @param integer output limit
	 * @param integer record offset
	 * @return JSON
	 */
	function select_table($table, $limit = 10, $offset = 0) {
		$this->groonga->resetAll();
		$this->groonga->setLimit($limit, $offset);
		$this->groonga->setTable($table);
		return $this->groonga->doSelect();
	}
	
	/**
	 * Get total records from table
	 * @access public
	 * @param string table name
	 * @return integer total records number
	 */
	function total_records($table) {
		$this->groonga->resetAll();
		$data = json_decode($this->select_table($table, 0));
		return intval($data[1][0][0][0]);
	}
	
	/**
	 * Get total stopwords
	 * @access public
	 * @return integer total stopwords number
	 */
	function total_stopwords() {
		$this->groonga->resetAll();
		return $this->total_records('stopwords');
	}
	
	/**
	 * Get total stopwords
	 * @access public
	 * @return mixed Returns array on success or FALSE on failed.
	 */
	function get_stopwords() {
		$this->groonga->resetAll();
		$data = json_decode($this->select_table('stopwords', $this->total_stopwords('stopwords')));
		$result = array();
		
		if(isset($data[1])){
			$rs = $data[1][0];
			for ($i=2; $i < count($rs); $i++) {
				$result[] = $rs[$i][2];
			}
		} else {
			return FALSE;
		}
		
		return $result;
	}
	
	/**
	 * Get migration last id
	 * @access public
	 * @return JSON 
	 */
	function get_lastid() {
		$this->groonga->resetAll();
		$this->groonga->setSortBy('-_id');
		$this->groonga->setLimit('1', '0');
		//$this->groonga->setOutputColumns('_key');
		$this->groonga->setTable('post_stream');
		return $this->groonga->doSelect();
	}
	
	/**
	 * Perform regular full text search
	 * @access public
	 * @param string search query or keywords
	 * @param string search type (OR => 'OR', AND => 'AND')
	 * @param array stopwords list
	 * @return JSON search results
	 */
	function regular_search($keyword, $label = '', $limit = '20', $page = '0') {
		$this->groonga->resetAll();
		
		$keyword = preg_replace('/[^a-zA-Z0-9]/', ' ', $keyword);

		switch ($label) {
			case 'article':
				$this->groonga->setMatchColumn('message');
				break;
			
			case 'title':
				$this->groonga->setMatchColumn('title');
				break;
				
			case 'tag':
				$this->groonga->setMatchColumn('tag');
				break;
				
			case 'source':
				$this->groonga->setMatchColumn('source');
				break;
			
			case '':
			default:
				$this->groonga->setMatchColumn(array('title','message'));
				break;
		}
		
		$this->groonga->setQuery($keyword);
		$this->groonga->setFilter('deleted==false');
		$this->groonga->setLimit($limit, ($limit * $page));
		$this->groonga->setTable('post_stream');
		$this->groonga->setSortBy('-creation_date,-_score');
		
		return $this->groonga->doSelect();
	}

	function engine_search($keyword, $label = '', $limit = '20', $page = '0') {
		$this->groonga->resetAll();
		
		$keyword = preg_replace('/[^a-zA-Z0-9\.]/', ' ', $keyword);

		switch ($label) {
			case 'article':
				$this->groonga->setMatchColumn('message');
				break;
			
			case 'title':
				$this->groonga->setMatchColumn('title');
				break;
			
			case 'tag':
				$this->groonga->setMatchColumn('tag');
				break;
			
			case 'source':
				$keyword = "source:@$keyword";
				break;
			
			case '':
			default:
				$this->groonga->setMatchColumn(array('title','message'));
				break;
		}
		
		$this->groonga->setQuery($keyword);
		$this->groonga->setFilter('deleted==false%26%26insert_by=="engine"'); 
		$this->groonga->setLimit($limit, ($limit * $page));
		$this->groonga->setTable('post_stream');
		$this->groonga->setSortBy('-creation_date,-_score');
		
		return $this->groonga->doSelect();
	}

	/**
	 * Perform tag search
	 * @access public
	 * @param string search query or keywords
	 * @param string search type (OR => 'OR', AND => 'AND')
	 * @param array stopwords list
	 * @return JSON search results
	 */
	function tag_search($keyword, $limit = '20', $page = '0') {
		$this->groonga->resetAll();
		
		$keyword = preg_replace('/[^a-zA-Z0-9\.]/', ' ', $keyword);
		
		$this->groonga->setMatchColumn('tag');
		$this->groonga->setQuery($keyword);
		$this->groonga->setFilter('deleted==false');
		$this->groonga->setLimit($limit, ($limit * $page));
		$this->groonga->setTable('post_stream');
		$this->groonga->setSortBy('-creation_date,-_score');
		
		return $this->groonga->doSelect();
	}
	
	/**
	 * Perform related post search using title as keyword and tag name
	 * 
	 * @access public
	 * @param string search query or keywords
	 * @param string search type (OR => 'OR', AND => 'AND')
	 * @param array stopwords list
	 * @return JSON search results
	 * 
	 */
	function related_post($keyword, $tag = '', $stype = '', $stopwords = array(), $limit = '20', $page = '0') {
		$this->groonga->resetAll();
		
		$keyword = preg_replace('/[^a-zA-Z0-9]/', ' ', $keyword);	
		$keywords = explode(' ', $keyword);
		$query1 = array();
		$query2 = array();
		
		foreach ($keywords as $value) {
			$value = trim($value,'"\',. ');
			if(count($stopwords) > 0 && !preg_grep('/'.$value.'/i', $stopwords) && strlen($value) > 0) {
				$query1[] = 'title:@' . $value; // . ' + tag:@' . $tag;
			}
		}
				
		$output_cols = array('_id','_key','creation_date','deleted','insert_dt','link_url','message','popular_post','source','tag','tipe','title','writer','_score');
		
		$this->groonga->setCondition($query1, $stype);
		
		if($tag != '') {
			$query2[] = 'tag:@' . $tag;
			$this->groonga->setCondition($query2, 'AND');
		}

		$this->groonga->setOutputColumns($output_cols);
		$this->groonga->setFilter('deleted==false');
		$this->groonga->setLimit($limit, ($limit * $page));
		$this->groonga->setTable('post_stream');
		$this->groonga->setSortBy('-creation_date, -_score');
	
		return $this->groonga->doSelect();
	}
	
	/**
	 * Perform suggestion from given query or keyword
	 * @access public
	 * @param string query or keywords
	 * @return JSON suggestion results
	 */
	function suggestion($query) {
		$this->groonga->resetAll();
		$this->groonga->setTable('item_related_post');
		$this->groonga->setSuggestColumn('kana');
		$this->groonga->setQuery($query);
		
		return $this->groonga->doSuggest();
	}
	
	/**
	 * Insert data to table. For regular fulltext search.
	 * This function also can be used to update existing record if key or id is already exist.
	 * @access public
	 * @param string table name
	 * @param array data to insert
	 * @return JSON status from groonga server
	 */
	function insert($table, $data) {
		$this->groonga->resetAll();		
		$this->groonga->setTable($table);
		$this->groonga->setInsertData($data);
		
		return $this->groonga->doInsert();
	}
	
	/**
	 * Delete data from table
	 * @access public
	 * @param string Table name
	 * @param string Record key
	 * @return JSON status from groonga server
	 */
	function delete($table, $key) {
		$this->groonga->resetAll();
		$this->groonga->setTable($table);
		$this->groonga->setKey($key);
		
		return $this->groonga->doDelete();
	}
	
	/**
	 * Insert learning data for suggestion
	 * @access public
	 * @param array data
	 * @return JSON status
	 */
	function insert_learning_data($data) {
		$this->groonga->resetAll();
		$this->groonga->setSuggestField(array('_id', 'type', 'item', 'sequence', 'time', 'pair_related_post'));
		$this->groonga->setTable('event_related_post');
		$this->groonga->setInsertData($data);
		
		return $this->groonga->doInsertLearningData();
	}
	
}

/**
 * End of file content_model.php
 * Location ./apps/models/content_model.php
 * 
 */